#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include "camera.h"
#include "tracer.h"
#include "pq.h"

#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "stb_image_write.h"

int main(int argc, char** argv){	

	readPSQL();
	
	uint8_t* bgr = new uint8_t[800*600*3];

	camera c;
	c._look[1] = -0.5f;
	c.normalize(c._look);
	
	tracer tr;
	float* rays = new float[800*600*6];
	
	c.generateRays(rays,800*600*6);
	usleep(10);
	while(true){
		int sph_size = 0;
		float* sph_data = loadScene(sph_size);
		
		if(sph_data == NULL){
			break;
		}
		
		tr._stage.clear();
		tr._stage.init(sph_data,sph_size);
		
		tr.trace(rays,800*600,bgr);

		stbi_write_jpg("../web/result.jpg", 800, 600, 3, bgr, 100);
	}
	tr._stage.clear();
	tr.clear();
	
	delete[] bgr;
	delete[] rays;

	return 0;
}
