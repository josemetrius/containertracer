#ifndef CAMERA_H
#define CAMERA_H

#include <math.h>
#include "vec3func.h"

class camera
{
public:
	float _dim = 2.0f;
	float _ortho[4] = {-_dim*(4.0f/3.0f),_dim*(4.0f/3.0f),_dim,-_dim};//LR UP
	int _width = 800;
	int _height = 600;
	
	float _center[3] = {0.0f,20.0f,40.0f};
	float _look[3] = {0.0f,0.0f,-1.0f};//forward
	float _up[3] = {0.0f,1.0f,0.0f};
	
	//length in floats 6*num rays
	void generateRays(float* rays, int lenght){
		if(lenght < _width*_height*6)
			return;
		
		float pixelSize[2] = {
			(_ortho[1]-_ortho[0])/((float)_width),
			(_ortho[2]-_ortho[3])/((float)_height),
		};
		
		float right[3];
		float up[3];
		
		
		V3Cross(_look,_up,right);
		V3Cross(right,_look,up);
		
		for(int j=0;j<_height;++j){
			for(int i=0;i<_width;++i){
				int rid = (_height-1-j)*_width+i;
				
				float* origin = &(rays[rid*6+0]);
				float* direction = &(rays[rid*6+3]);
				
				float dx = (_ortho[0]+((float)i)*pixelSize[0]);
				float dy = (_ortho[3]+((float)j)*pixelSize[1]);
				
				V3Mult(right,dx,origin);
				V3Mad(up,dy,origin,origin);	
				V3Add(_center,origin,origin);
				V3Copy(_look,direction);
			}
		}
		
		
	}
	
	void normalize(float* vec){
		float mag = sqrtf(vec[0]*vec[0] + vec[1]*vec[1] + vec[2]*vec[2]);
		vec[0]/=mag;
		vec[1]/=mag;
		vec[2]/=mag;
	}
	
};






#endif