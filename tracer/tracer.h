#ifndef TRACER_H
#define TRACER_H

#include <math.h>
#include <random>
#include "radixF32.h"
#include "scene.h"
#define PI 3.1415

class tracer
{
public:
	union fbit{
		float f;
		uint32_t ui;
	};
	
	scene _stage;
	radix _r;
	
	int max_depth = 1;
	
	float* color = NULL;
	int cycle = 0;

	inline void sampleCorrect(float* nor, float* out){
		int res = 2000000000;
		
		float r1 = rand()%res;
		float r2 = rand()%res;
		
		r1 /= (float)res;
		r2 /= (float)res;
		
		float sinTheta = sqrtf(1.0f - r1*r1); 
		float phi = 2*PI*r2; 
		
		float samp[3];
		samp[0] = sinTheta*cosf(phi);
		samp[1] = 1.0f;
		samp[2] = sinTheta*sinf(phi);
		
		float n2[3];
		V3Mult(nor,nor,n2);
		float tgt[3];
		float btg[3];
		
		if(fabsf(nor[0]) > fabsf(nor[1])){
			tgt[0] = nor[2];
			tgt[1] = 0.0f;
			tgt[2] = -nor[0];
			
			V3Mult(tgt, sqrtf(n2[0]+n2[2]),tgt);
		}else{
			tgt[0] = 0.0f;
			tgt[1] = -nor[2];
			tgt[2] = nor[1];
			
			V3Mult(tgt, sqrtf(n2[1]+n2[2]),tgt);
		}
		V3Cross(nor,tgt,btg);
		
		float m0[3] = {tgt[0],nor[0],btg[0]};
		float m1[3] = {tgt[1],nor[1],btg[1]};
		float m2[3] = {tgt[2],nor[2],btg[2]};

		out[0] = V3Dot(samp,m0);
		out[1] = V3Dot(samp,m1);
		out[2] = V3Dot(samp,m2);
	}

	inline bool hitTest(float* rOrigin, float* rDirection, float* sCenter, float sRadius2, float* hit, float* normal){	
		float l[3];
		V3Subt(sCenter,rOrigin,l);
		float tca = V3Dot(l,rDirection);
		float d2 = sRadius2 -V3Dot(l,l) +tca*tca;
		if(d2 < 0.0f)
			return false;
			
		float thc = sqrtf(d2);
		
		float t0 = tca-thc;
		float t1 = tca+thc;
		
		if(t0 > t1){
			float b = t0;
			t0 = t1;
			t1 = b;
		}
		if(abs(abs(t0)-0.000002f) < 0.000002f){
			t0 = t1;
		}
		
		if(t0 < 0.0f)
			return false;
			
		V3Mad(rDirection,t0,rOrigin,hit);
		V3Subt(hit,sCenter,normal);
		V3Norm(normal);
		
		return true;
	}
	inline void minDistance(float* sphere, int count, float* rOrigin, float* dist){
		for(int i=0;i<count;++i){
			float r = sphere[i*4+3];
			dist[i] = V3Dist2(&(sphere[i*4]),rOrigin)-r*r;
		}
	}
	inline void intersects(float* sphere, int count, float* rOrigin, float* rDirection, float* dist){
		float ap[3];
		float n[3];
		
		for(int i=0;i<count;++i){
			V3Subt(&(sphere[i*4]),rOrigin,ap);
			V3Cross(ap,rDirection,n);
			float r = sphere[i*4+3];
			if((r*r-V3Dot(n,n)) < 0.0f)
				dist[i] = 10000000.0f;
		}
	}
	
	bool rayScene(float* sphere, int count, float* rOrigin, float* rDirection, float* t){

		for(int i=0;i<count;++i){
			float* sCenter = &(sphere[i*4]);
			float* sRadius = &(sphere[i*4+3]);
			
			float l[3];
			V3Subt(sCenter,rOrigin,l);
			float tca = V3Dot(l,rDirection);
			float d2 = sRadius[0]*sRadius[0] -V3Dot(l,l) +tca*tca;
			if(d2 < 0.0f){
				t[i] = 10000000.0f;
				continue;
			}
				
			float thc = sqrtf(d2);
			
			float t0 = tca-thc;
			float t1 = tca+thc;
			
			if(t0 > t1){
				float b = t0;
				t0 = t1;
				t1 = b;
			}
			if(abs(abs(t0)-0.000002f) < 0.000002f){
				t0 = t1;
			}
			
			if(t0 < 0.0f){
				t[i] = 10000000.0f;
				continue;
			}
			t[i] = t0;
		}
	}
	
	bool raySphere(float* rOrigin, float* rDirection, float* hit, float* nor, int skip, int& obj){
		rayScene(_stage._sphere,_stage._size,rOrigin,rDirection,_stage._dist);
		_r.sort(_stage._dist,_stage._size,_stage._index);
		int ind = _stage._index[0];
		float t = _stage._dist[ind];
		if(t > 10000.0f)
			return false;
		
		V3Mad(rDirection,t,rOrigin,hit);
		V3Subt(hit,&(_stage._sphere[ind*4]),nor);
		V3Norm(nor);
		obj = ind;
		
		return true;
	}
	
	inline bool shadowRay(int lightId, int mat, float* rDir, float* rHit, float* rNor, float* out){
		float sDir[3];
		
		V3Subt(&(_stage._light[3*lightId]),rHit,sDir);
		V3Norm(sDir);
		
		V3Zero(out);
		
		//oposite side of the sphere
		if(V3Dot(sDir,rNor) <= 0.0f)
			return false;
		
		float hit[3];
		float nor[3];
		int mt;
		
		
		bool ocluded = raySphere(rHit,sDir,hit,nor,0, mt);
		if(!ocluded){
			float w0[3];
			V3Copy(rDir,w0);
			V3Neg(w0);
			
			//first 400 as intensity on white light 1,1,1
			float att = 400.0f / (4.0f * PI * V3Dist2(rHit,&(_stage._light[lightId])));

			V3One(out);
			V3Mult(out,V3Dot(rNor,sDir)*att,out);
		}
		return !ocluded;
	}

	
	inline bool rayTrace(float* rOri, float* rDir, int depth, float* out){
		V3Zero(out);
		
		if(depth > max_depth)
			return false;
		
		float oHit[3];
		float oNor[3];
		int tgt;
		
		if(!raySphere(rOri,rDir,oHit,oNor,0, tgt))
			return false;
		
		float lAcc[3];
		V3Zero(lAcc);
		
		float* albedo = &(_stage._albedo[tgt*3]);
		int lCount = 1;
		
		//direct light
		float lOut[3];
		V3Zero(lOut);
		float lRatio = 1.0f/((float)lCount);
		for(int i=0;i<lCount;++i){
			float pOut[3];
			shadowRay(i,tgt,rDir,oHit,oNor,pOut);
			V3Mult(pOut,lRatio,pOut);
			V3Add(lOut,pOut,lOut);
		}
		
		const float p = 2.0f*PI;
		
		
		float sOut[3];
		V3Zero(sOut);
		if(depth < max_depth){
			int sCount = 8/(depth+1);
			float sRatio = 1.0f/((float)sCount);
			
			//indiredct light
			for(int i=0;i<sCount;++i){
				float pOut[3];
				float sDir[3];
				
				sampleCorrect(oNor,sDir);
				if(rayTrace(oHit,sDir,depth+1,pOut)){
					
					float cosAng = V3Dot(sDir,oNor);
					
					V3Mult(pOut,sRatio*cosAng*p,pOut);
					V3Add(sOut,pOut,sOut);
				}
			}
		}
		
		V3Add(lOut,sOut,out);
		V3Mult(out,albedo,out);
		V3Mult(out,1.0f/PI,out);
		
		
		return true;
	}
	
	
	void trace(float* ray, int raycount, uint8_t* col){
		if(color == NULL)
		{
			color = new float[raycount*3];
			for(int i=0;i<raycount*3;++i){
				color[i] = 0.0f;
			}
		}
		
		bool compress = false;
		if(cycle == 10){
			compress = true;
			cycle = 5;
		}
		
		for(int i=0;i<raycount;++i){
			col[i*3+2] = 0;
			col[i*3+1] = 0;
			col[i*3+0] = 80;
			
			float hit[3];
			float nor[3];
			int tgt;
			float* rOri = &(ray[i*6+0]);
			float* rDir = &(ray[i*6+3]);
			
			float fcol[3];
			
			rayTrace(rOri,rDir,0,fcol);
			
			if(fcol[0] > 1.0f) fcol[0] = 1.0f;
			if(fcol[1] > 1.0f) fcol[1] = 1.0f;
			if(fcol[2] > 1.0f) fcol[2] = 1.0f;
			
			float* pCol = &(color[i*3]);
			if(compress){
				V3Mult(pCol,0.5f,pCol);
			}
			V3Add(fcol, pCol, pCol);
			float loops = (float)(cycle+1);
			V3Mult(pCol,1.0f/loops,fcol);
			
			V3Mult(fcol,255.0f,fcol);
			
			col[i*3+2] = fcol[0];
			col[i*3+1] = fcol[1];
			col[i*3+0] = fcol[2];
		}
		
		++cycle;
	}
	
	void clear(){
		delete[] color;
	}
	
};

#endif
