#ifndef VEC3FUNC_H
#define VEC3FUNC_H

#include <math.h>

#define PI 3.14159265

inline float* V3Zero(float* out){
	out[0] = out[1] = out[2] = 0.0f;
	return out;
}
inline float* V3One(float* out){
	out[0] = out[1] = out[2] = 1.0f;
	return out;
}

inline float* V3Neg(float* in){
	in[0] = -in[0];
	in[1] = -in[1];
	in[2] = -in[2];
	return in;
}

inline float* V3Add(float* in1, float* in2, float* out){
	out[0] = in1[0] + in2[0];
	out[1] = in1[1] + in2[1];
	out[2] = in1[2] + in2[2];
	return out;
}
inline float* V3Subt(float* in1, float* in2, float* out){
	out[0] = in1[0] - in2[0];
	out[1] = in1[1] - in2[1];
	out[2] = in1[2] - in2[2];
	return out;
}
inline float* V3Mult(float* in1, float in2, float* out){
	out[0] = in1[0] * in2;
	out[1] = in1[1] * in2;
	out[2] = in1[2] * in2;
	return out;
}
inline float* V3Mult(float* in1, float* in2, float* out){
	out[0] = in1[0] * in2[0];
	out[1] = in1[1] * in2[1];
	out[2] = in1[2] * in2[2];
	return out;
}
inline float* V3Mad(float* in1, float* in2, float* in3, float* out){
	out[0] = in1[0] * in2[0] + in3[0];
	out[1] = in1[1] * in2[1] + in3[1];
	out[2] = in1[2] * in2[2] + in3[2];
	return out;
}
inline float* V3Mad(float* in1, float in2, float* in3, float* out){
	out[0] = in1[0] * in2 + in3[0];
	out[1] = in1[1] * in2 + in3[1];
	out[2] = in1[2] * in2 + in3[2];
	return out;
}
inline float* V3Cross(float* in1, float* in2, float* out){
	out[0] = (in1[1] * in2[2]) - (in1[2] * in2[1]);
	out[1] = (in1[2] * in2[0]) - (in1[0] * in2[2]);
	out[2] = (in1[0] * in2[1]) - (in1[1] * in2[0]);
	return out;
}
inline float V3Dot(float* in1, float* in2){
	return ((in1[0]*in2[0])+(in1[1]*in2[1])+(in1[2]*in2[2]));
}
inline float* V3Refl(float* d, float* n, float* out){
	return V3Mad(n,-2.0f*V3Dot(d,n),d,out);
}

inline float V3Mod(float* in){
	return sqrt(pow(in[0],2)+pow(in[1],2)+pow(in[2],2));
}

inline float* V3Copy(float* in, float* out){
	out[0] = in[0];
	out[1] = in[1];
	out[2] = in[2];
	return out;
}

inline float* V3RotX(float* in1, float in2, float* out){
	in2 = in2*(PI/180.0f);
	out[0] = in1[0];
	out[1] = in1[1]*cos(in2) - in1[2]*sin(in2);
	out[2] = in1[1]*sin(in2) + in1[2]*cos(in2);
	return out;
}
inline float* V3RotY(float* in1, float in2, float* out){
	in2 = in2*(PI/180.0f);
	out[0] = in1[2]*sin(in2) + in1[0]*cos(in2);
	out[1] = in1[1];
	out[2] = in1[2]*cos(in2) - in1[0]*sin(in2);
	return out;
}
inline float* V3RotZ(float* in1, float in2, float* out){
	in2 = in2*(PI/180.0f);
	out[0] = in1[0]*cos(in2) - in1[1]*sin(in2);
	out[1] = in1[0]*sin(in2) + in1[1]*cos(in2);
	out[2] = in1[2];
	return out;
}

inline float* V3RotXYZ(float* in1, float* in2, float* out){
	float buf[3];
	V3RotX(in1,in2[0],out);
	V3RotY(out,in2[1],buf);
	V3RotZ(buf,in2[2],out);
	return out;
}
inline float* V3RotZXY(float* in1, float* in2, float* out){
	float buf[3];
	V3RotZ(in1,in2[2],out);
	V3RotX(out,in2[0],buf);
	V3RotY(buf,in2[1],out);
	return out;
}

//wrappers
inline float V3Dist2(float* in1, float* in2){
	float temp[3];
	V3Subt(in2,in1,temp);
	return V3Dot(temp,temp);
}
inline float* V3Norm(float* in1){
	return V3Mult(
		in1,
		1.0f/sqrtf(V3Dot(in1,in1)),
		in1
	);
}

#endif