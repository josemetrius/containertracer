#ifndef SCENE_H
#define SCENE_H
#include <random>

class scene
{
public:
	float _amb[3] = {
		0.01f,0.04f,0.08f
	};
	float* _sphere = NULL;
	float* _albedo = NULL;
	float* _prop = NULL;
	float _light[1*4] = {//x,y,z,r
		2.0f,5.0f,1.0f, 0.5f
	};
	float* _dist = NULL;
	int* _index = NULL;
	int _size = 0;
	
	void init(float* sphere, int count){
		_sphere = sphere;
		_size = count;
		
		if(count == 0) return;
		
		_albedo = new float[count*3];
		_prop = new float[count*2];
		
		_dist = new float[count];
		_index = new int[count];
		
		//store seed
		int r = rand();
		srand(0);
		
		for(int i=0;i<count;++i){
			_albedo[i*3+0] = (rand()%10000)/10000.0f;
			_albedo[i*3+1] = (rand()%10000)/10000.0f;
			_albedo[i*3+2] = (rand()%10000)/10000.0f;
			_prop[i*2+0] = 0.05f;
			_prop[i*2+1] = 0.9f;
			_dist[i] = 0.0f;
			_index[i] = 0;
		}
		
		_albedo[0] = 0.92f;
		_albedo[1] = 0.8f;
		_albedo[2] = 0.75f;
		
		//restore seed
		srand(r);
	}	
	
	void clear(){
		if(_sphere != NULL) delete[] _sphere;
		if(_albedo != NULL) delete[] _albedo;
		if(_prop != NULL) delete[] _prop;
		if(_dist != NULL) delete[] _dist;
		if(_index != NULL) delete[] _index;
	}
	
};
#endif
