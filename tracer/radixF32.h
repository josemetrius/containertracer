#ifndef RADIX_F32_H
#define RADIX_F32_H

#include <string.h>
#include <stdint.h>
#include <algorithm>
#include <numeric>

class radix
{
public:
	void sort(float* flts, int count, int* index){
		std::iota(index,index+count,0);
		std::stable_sort(index,index+count,
			[&flts](size_t lhs, size_t rhs){
				return flts[lhs] < flts[rhs];
			}
		);
		
	}
};
#endif
