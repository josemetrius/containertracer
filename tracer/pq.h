#ifndef PQ_H
#define PQ_H

#include <stdio.h>
#include <postgresql/libpq-fe.h>
#include <string>
#include <fstream>

void strToFile(std::string str){
	std::ofstream out("log.txt");
	out.write(str.c_str(),str.size());
	out.close();
}

void readPSQL(){
	PGconn *conn;
	PGresult *res;
	int rec_count;
	
	std::string result = ""; 
	
	conn = PQconnectdb("dbname=scene host=db port=5432 user=postgres");
	
	
	if(PQstatus(conn) == CONNECTION_BAD) {
		result += "We were unable to connect to the database";
		return strToFile(result);
	}
	
	//res = PQexec(conn, "update people set phonenumber=\'5055559999\' where id=3");
	res = PQexec(conn, "select x,y,z,r from sphere order by id");//"select lastname,firstname,phonenumber from people order by id");

	if(PQresultStatus(res) != PGRES_TUPLES_OK) {
		result += "We did not get any data!";
		return strToFile(result);
	}
	
	rec_count = PQntuples(res);
	
	result += "We received ";
	result += std::to_string(rec_count);
	result += " records.\n";

	result += "===============\n";
	for(int row=0; row<rec_count; row++) {
		for(int col=0; col<4; col++) {
			result += PQgetvalue(res, row, col);
			result += "\t";
		}
		result += "\n";
	}
	result += "===============\n";
	
	strToFile(result);

	PQclear(res);
	PQfinish(conn);
}

float* loadScene(int& count){
	count = 0;
	
	PGconn *conn = PQconnectdb("dbname=scene host=db port=5432 user=postgres");
	
	PGresult *res;
	if(PQstatus(conn) == CONNECTION_BAD) {
		return NULL;
	}
	res = PQexec(conn, "select x,y,z,r from sphere order by id");//"select lastname,firstname,phonenumber from people order by id");

	bool isData = true;	
	
	if(PQresultStatus(res) != PGRES_TUPLES_OK) {
		isData == false;
	}
	if(isData){
		count = PQntuples(res);
	}
	if(count == 0){
		PQclear(res);
		PQfinish(conn);
		isData = false;
	}
	float* sphere = new float[(count+1)*4];
	sphere[0] = 0.0f;
	sphere[1] = -100.0f;
	sphere[2] = 0.0f;
	sphere[3] = 100.0f;
	if(isData){
		for(int i=0; i<count; ++i){
			for(int j=0; j<4; ++j){
				std::string flt = PQgetvalue(res, i, j);
				float f;
				sscanf(flt.c_str(),"%f",&f);
				sphere[(i+1)*4+j] = f;
			}
		}
	}
	++count;
	
	if(isData){
		PQclear(res);
		PQfinish(conn);
	}
	return sphere;
}



#endif
