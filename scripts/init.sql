CREATE DATABASE scene;
\c scene

CREATE TABLE sphere (
	id serial not NULL,
	time TIMESTAMP not null DEFAULT CURRENT_TIMESTAMP,
	x FLOAT(24) not NULL,
	y FLOAT(24) not NULL,
	z FLOAT(24) not NULL,
	r FLOAT(24) not NULL
);
