# ContainerTracer

Ray Tracer progresivo com iluminação global Lambert.

## Microserviços:
 - web:nginx
 - app:python
 - db:postgres
 - tracer:gcc

## Comunicações:
 - web -> app (adicionar objeto)
 - app -> db (salvar no db objeto)
 - db -> tracer (carregar cena)
 - tracer -> web (salva resultado parcial)

## Dependencias:
 - Docker https://www.docker.com/
 - Docker-Compose https://docs.docker.com/compose/

## Execução:
 - Run "docker-compose up -d".
 - Acesso atraves do "localhost" no navegador da web.
 	
## Commandos:
 - Debug bando de dados: "docker-compose exec db psql -U postgres -d scene -c 'SELECT * from sphere'"
 - Para serviço: "docker-compose down".
 - Resetar bando de dados: após parar serviço "docker volume rm docker_dados"

## Informações:
 - Devido a carga de dependencias do python o micro serviço que envia dados para o servidor leva um pocuo a mais de tempo para ser levantado. Para ter certeza use: "docker-compose up" para iniciar o serviço.

## Relatorio:
 - [Relatorio tecnico](Relatorio.pdf)
