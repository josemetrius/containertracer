import psycopg2
from bottle import route, run, request, redirect

DSN = 'dbname=scene user=postgres host=db'
SQL = 'INSERT INTO sphere (x, y, z, r) VALUES (%s, %s, %s, %s)'

def registro_pedido(cx,cy,cz,cr):
	connecta = psycopg2.connect(DSN)
	cursosql = connecta.cursor()
	cursosql.execute(SQL, (cx,cy,cz,cr))
	connecta.commit()
	cursosql.close()
	
	print('Mensagem registrada.')

@route('/', method='POST')
def send():
	cx = request.forms.get('cx')
	cy = request.forms.get('cy')
	cz = request.forms.get('cz')
	cr = request.forms.get('cr')
		
	registro_pedido(cx,cy,cz,cr)
	
	redirect('http://localhost/view.html');
	
	return 'Registered sphere: Center: {} {} {} Radius: {} '.format(#<script>setInterval(function(){window.location.href = "/:80";}, 5000);</script>'.format(
		cx,cy,cz,cr
	)
	
if __name__ == '__main__':
	run(host='0.0.0.0', port=8080, debug=True)
